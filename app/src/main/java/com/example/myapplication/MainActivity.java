package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView numero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numero = (TextView)findViewById(R.id.textView);

    }

    // BOTONES

    public void Boton1(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "1";
        numero.setText(calculo);
    }
    public void Boton2(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "2";
        numero.setText(calculo);
    }
    public void Boton3(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "3";
        numero.setText(calculo);
    }
    public void Boton4(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "4";
        numero.setText(calculo);
    }
    public void Boton5(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "5";
        numero.setText(calculo);
    }
    public void Boton6(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "6";
        numero.setText(calculo);
    }
    public void Boton7(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "7";
        numero.setText(calculo);
    }
    public void Boton8(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "8";
        numero.setText(calculo);
    }
    public void Boton9(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "9";
        numero.setText(calculo);
    }
    public void Boton0(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "0";
        numero.setText(calculo);
    }
    public void Boton_suma(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "+";
        numero.setText(calculo);
    }
    public void Boton_resta(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "-";
        numero.setText(calculo);
    }
    public void Boton_multi(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "x";
        numero.setText(calculo);
    }
    public void Boton_div(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + "/";
        numero.setText(calculo);
    }
    public void Boton_punto(View view) {
        String calculo = numero.getText().toString();
        calculo = calculo + ".";
        numero.setText(calculo);
    }

    //BOTON DE IGUAL
    public void Boton_igual(View view) {
        String calculo = numero.getText().toString();
        double cal = Operador(calculo);
        if (cal>=1){
            if (cal%1==0){
                int cal2;
                cal2=(int)cal;
                String resultado = String.valueOf(cal2);
                numero.setText(resultado);
        }
        }else{
            String resultado = String.valueOf(cal);
            numero.setText(resultado);
        }
    }

    //BOTON DE BORRAR

    public void Boton_borrar(View view) {
        String calculo = numero.getText().toString();
        String new_calculo="";
        for (int i = 0; i <calculo.length()-1 ; i ++){
            char cal = calculo.charAt(i);
            new_calculo = new_calculo + cal;
        }
        numero.setText(new_calculo);
    }



    public Double Operador(String calculo){

        List<Double> numeros = new ArrayList<Double>();
        String operadores = "";
        String numero="";
        double cal= 0;
        int i;

        //  CREANDO LA LISTA DE NUMEROS Y CADENA DE OPERADORES
        for (i = 0; i <calculo.length() ; i ++){

            if (calculo.charAt(i) == '+'){
                if (numero!=""){
                    numeros.add(Double.parseDouble(numero));
                    operadores= operadores + '+';
                }
                numero = "";
            }else if(calculo.charAt(i) == '-'){
                if (numero!=""){

                    numeros.add(Double.parseDouble(numero));
                    operadores= operadores + '-';
                }else if (i==0){
                    numero="0";
                    numeros.add(Double.parseDouble(numero));
                    operadores= operadores + '-';
                }


                numero = "";
            }else if(calculo.charAt(i) == 'x'){
                if (numero!=""){
                    numeros.add(Double.parseDouble(numero));
                    operadores= operadores + 'x';
                }
                numero = "";
            }else if(calculo.charAt(i) == '/'){
                if (numero!=""){
                    numeros.add(Double.parseDouble(numero));
                    operadores= operadores + '/';
                }
                numero = "";
            }
            else{
                numero = numero + calculo.charAt(i);
            }

        }

        numeros.add(Double.parseDouble(numero));

        //OPERANDO
        double cal3;

        String operadores2="";
        for (int j=0 ;j<operadores.length() ; j ++){
            if (operadores.charAt(j)=='x'){
                cal3 = numeros.get(j) * numeros.get(j+1);
                numeros.add(j,cal3);


            }else if (operadores.charAt(j)=='/'){
                cal3 = numeros.get(j) / numeros.get(j+1);
                numeros.add(j,cal3);

            }else{
                operadores2 = operadores2 + operadores.charAt(j);
            }

        }

        for (i=0 ; i <operadores2.length() ; i ++){
            if (operadores2.charAt(i)=='+'){
                if (i==0){
                    cal = numeros.get(i) + numeros.get(i+1);
                }else {
                    cal = cal + numeros.get(i+1);
                }
            }else if (operadores2.charAt(i)=='-'){
                if (i==0){
                    cal = numeros.get(i) - numeros.get(i+1);
                }else {
                    cal = cal - numeros.get(i+1);
                }
            }
        }


        if (cal==0){
            cal = numeros.get(0);
        }
        numeros.clear();

        return cal;
    }
}
